#include <iostream>
#include <string>
#include <fstream>
#include <vector>

using namespace std;
void print_error(string);
void print_error(string line) {
	cout << line << endl;
}

int main (int argc, char **argv) {
	ifstream infile;
	ofstream outfile;
	vector<string> contents;
	int i = 0;				//vector index
	int j = 0;
	if (argc != 2) {
		print_error("oops");
		return 1;
	}
	infile.open(argv[1], ios::in);
	if (!infile) {
		print_error("...");
		return 1;
	}
	string line;
	while (!infile.eof()) {
		getline(infile, line);
		contents.push_back(line);
	}
	infile.close();

	for (i; i < contents.size(); i++)	//print contents of file
		cout << contents[i] << endl;

	outfile.open("output.txt", ios::out);
	if (!outfile) {
		print_error("...");
		return 1;
	}
	for(i = 0; i < contents.size(); i++)
		outfile << contents[i] << endl;
	outfile.close();
}
