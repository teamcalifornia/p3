#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <iomanip>
#include "makeComment.h"
#include "makeOperand.h"

using namespace std;

void print_error(string);
vector<string> contents;

void print_error(string line) {
	cout << line << endl;
}
string get_token(unsigned int, unsigned int);

int main (int argc, char **argv) {
	ifstream infile;
	ofstream outfile;
	int i = 0;				//vector index

	if (argc != 2) {
		print_error("oops");
		return 1;
	}
	infile.open(argv[1], ios::in);
	if (!infile) {
		print_error("...");
		return 1;
	}
	string line;
	while (!infile.eof()) {
		getline(infile, line);
		contents.push_back(line);
	}
	contents.pop_back();
	infile.close();
	cout << "********************** CONTENTS LOOP *****************************" << endl;

	for (i = 0; i < contents.size(); i++)	//print contents of file
		cout << i << " " << contents[i] << endl;

	cout << "******************************************************************" << endl;

	//outfile.open("output.txt", ios::out);
	//if (!outfile) {
	//	print_error("...");
	//	return 1;
	//}
	//for(i = 0; i < contents.size(); i++)
	//	outfile << contents[i] << endl;
	//outfile.close();


	cout << "************************ TOKENS LOOP *****************************" << endl;

	for (i = 0; i < contents.size(); i++) {	//print contents of file

		cout << setw(3) << left << i << " " << 
			setw(14) << left << get_token(i,0) <<
			setw(14) << left << get_token(i,1) <<
			setw(50) << left << get_token(i,2) <<
			setw(50) << left << get_token(i,3) << endl;

	}

cout << contents[3] << endl;
exit(0);
	cout << "******************************************************************" << endl;

	cout << "Token 0: " << get_token(14,2) << endl;
	cout << "Token 1: " << get_token(15,2) << endl;
	cout << "Token 2: " << get_token(16,2) << endl;
	cout << "Token 3: " << get_token(17,2) << endl;
	cout << "Token 4: " << get_token(18,2) << endl;
}

string get_token(unsigned int row, unsigned int column) {
	int current_column; 
	char buffer[100];
	char line_copy[100];
	char delim[] = " \t";

	/*	Check for invalid rows and columns	*/
	if(row < 0 || row >= contents.size())
		return "Invalid row";
	if(column < 0 || column > 3)
		return "Invalid column";
	if(contents[row].empty()) return "";
	/*	If the first character in line is	*/
	/*	a letter or a period ( . ), start	*/
	/*	at column 0. Otherwise start column 1	*/
	if(isalpha(contents[row][0]) || contents[row][0] == '.') 
		current_column = 0;
	else
		current_column = 1;

	/*	Buffers to hold current token and	*/
	/*	a copy of the line		*/
	strcpy(buffer, contents[row].c_str());
	strcpy(line_copy, buffer);

	/*	Create tokens based on current col. 	*/
	/*	number. If a period is found before	*/
	/*	you arrive at target column, return	*/
	/*	the emtpy string.			*/
	char *ptr = strtok(buffer, delim);
	while (current_column != column && ptr != NULL) { 

		if(*ptr == '.')
			if( column == 3 ) return make_comment(line_copy);
			else return "";

		ptr = strtok(NULL, delim);
		current_column++;

	}
	/*	MALIK, changed the if statement above	*/
	/*	Now it only prints out comment ...	*/
	/*	... if column 3 is asked for		*/

	/*	If at column 0, check for start of a 	*/
	/*	label or beginning of comment. 		*/
	/*	Otherwise, return empty string.		*/
	if (column == 0) { 
		if(isalpha(contents[row][0]) && contents[row][0] != '.') 
               		return ptr;
		//else if (contents[row][0] == '.')
		//	return make_comment(line_copy); 
		//return "";
	}
	/* 	MALIK, I commented the above code.	*/
	/*	It was displaying the comment ...	*/
	/* 	... but column 0 means label ....	*/
	/*	... return empty if no label		*/

	/*	If at columns 1 or 2, check for 	*/
	/*	beginning of comment. Otherwise		*/
	/*	return token at ptr.			*/
	else if ((column == 1 || column == 2) && (*ptr != '.')) {
		//if(*ptr == '.')
		//	return make_comment(line_copy); 
		if (column == 2)
			return make_operand(line_copy);
		else
			return ptr;
	}
	/*	MALIK, same with the above ...		*/
	/*	I commended the make_comment out...	*/
	/*	.. if column 1, then return opcode	*/
	/*	if no opcode in line, return ""		*/
	/*	.. if column 2, then return operand	*/
	/*	if no operand in line return ""		*/
	
	/*	If at column 3 and ptr is NULL return	*/
	/*	empty string. If at column 3 and the 	*/
	/*	first char into token is not a period	*/
	/*	return invalid token. Otherwise return 	*/
	/*	comment.				*/
	else if (column == 3) {
		if (ptr == NULL)
			return "";
		else if (*ptr != '.')
			return "Invalid comment";	
		return make_comment(line_copy); 
	}
	return " ";
}
