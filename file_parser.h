<<<<<<< HEAD
/*
    Mark Mandocdoc, Malik McElroy, Xiaojia Song, Mohamed Sharif 
    masc0882 
    Team California
    prog3
    CS530, Spring 2016
 */
=======
/*  file_parser.h
    CS530, Spring 2016
*/

/*  Team California
    Malik McElroy



*/
>>>>>>> 12812d65ca4e19374fee0ce42f6c88fe52799d8d

#ifndef FILE_PARSER_H
#define FILE_PARSER_H

<<<<<<< HEAD
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <iomanip>
#include <cstdlib>
#include <sstream>
=======
#include <string>
>>>>>>> 12812d65ca4e19374fee0ce42f6c88fe52799d8d

using namespace std;

class file_parser {
    public:
        // ctor, filename is the parameter.  A driver program will read
        // the filename from the command line, and pass the filename to
        // the file_parser constructor.  Filenames must not be hard-coded.
        file_parser(string); 
        
        // dtor
        ~file_parser();
        
        // reads the source file, storing the information is some
        // auxiliary data structure you define in the private section. 
        // Throws a file_parse_exception if an error occurs.
        // if the source code file fails to conform to the above
        // specification, this is an error condition.     
        void read_file();    
        
        // returns the token found at (row, column).  Rows and columns
        // are zero based.  Returns the empty string "" if there is no 
        // token at that location. column refers to the four fields
        // identified above.
        string get_token(unsigned int, unsigned int);
        
        // prints the source code file to stdout.  Should not repeat 
        // the exact formatting of the original, but uses tabs to align
        // similar tokens in a column. The fields should match the 
        // order of token fields given above (label/opcode/operands/comments)
        void print_file();
        
        // returns the number of lines in the source code file
        int size();
        
    private:
<<<<<<< HEAD
	struct line {
		string label;
		string label_full;
		string opcode;
		string operand;
		string comment;
		line() {
			label = label_full = opcode = operand = comment = "";
		}
	};

	string FILE_NAME;
	vector <string> contents;
	vector <line> LINES;

	string make_label(char *);
	string make_label_full(char *);
	string make_opcode(char *);
	string make_operand(char *);
	string make_comment(char *);
	
=======
        // your variables and private methods go here

>>>>>>> 12812d65ca4e19374fee0ce42f6c88fe52799d8d
};

#endif    
