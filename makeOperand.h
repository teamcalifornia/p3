#include <iostream>
#include <stdlib.h>
#include <string.h>
#include "file_parse_exception.h"

using namespace std;

string make_operand (char *s) {
	
	bool inOperand = false;
	bool inQuote = false;
	int columnCount = 0;
	int currentColumn = 0;
	string operand = "";
	int i = 0;

	while(s[i] != NULL && currentColumn != 2 && s[i] != '.') {
		while(isalpha(s[i])) i++;
		while(s[i] == ' ' || s[i] == '\t'){ i++; }
		currentColumn++;
	}
	if( s[i] == '.' || s[i+1] == '.' ) return "";

	while(s[i] != NULL) {
		if(s[i] == '\''  && inQuote == false) inQuote = true;
		else if(s[i] == '\'' && inQuote == true && (s[i+1] == ' ' || s[i+1] == '\t' || s[i+1] == NULL)) inQuote = false;
		operand.push_back(s[i]);
		i++;

		if((s[i] == ' ' || s[i] == '\t') && inQuote == false) break;
	}

	if(inQuote == true) { 
		throw file_parse_exception("Error: expected quotation");
	}

	return operand;
}
