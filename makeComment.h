#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;

string make_comment (char *s) {
	bool inComment = false;
	string comment = "";
	int i = 0;
	while(s[i] != NULL) {
		if(s[i] == '.')
			inComment = true;
		if(inComment)
			comment.push_back(s[i]);
		i++;
	}
	return comment;
}
