/*  Mark Mandocdoc, Malik McElroy, Xiaojia Song, Mohamed Sharif
    masc0882
    Team California
    prog3
    CS530, Spring 2016
*/

#include "sicxe_asm.h"

using namespace std;

int main(int argc, char *argv[]) {

	if (argc != 2) {
		cout << "Error. Filename must be given at command line." << endl;
		exit(1);
	}

	string filename = argv[1];
	string raw_filename = get_raw_filename(filename);

	// initialize parsed file data
	file_parser the_data(filename);

	// initialize symtab
	symtab symtab_data;

	// initialize opcode table
	opcodetab opcode_table;

	try {
		
		// initialize file parser with given file name	
		the_data.read_file();
		unsigned int addresses[the_data.size()];
		unsigned int locctr = 0;

		bool start_encountered = false, end_encountered = false;
		bool base_enabled = false;
		string label, opcode, operand, comment;
		
		// the label used with the start directive
		// will be compared once the end directive is reached
		string start_label;
			
		for(int i=0; i<the_data.size(); i++) {

			label = the_data.get_token(i,0);
			opcode = the_data.get_token(i,1);
			operand = the_data.get_token(i,2);
			comment = the_data.get_token(i,3);
			transform(opcode.begin(), opcode.end(),opcode.begin(), ::toupper);

			// if label is not empty, add label to symtab
			// pass string label, int address, and int line number for exception
			// and also if its not the EQU directive
			if(!label.empty() && get_uppercase(opcode) != "EQU") symtab_data.add_symbol_to_map(label, locctr, i+1);

			if(opcode == "START") {
				start_encountered = true;
				addresses[i] = locctr;

				locctr = handle_start(operand, i);

				// initialize label found at start directive
				start_label = get_uppercase(label);

				continue;
			}
			else if(is_blank_or_comment(label,opcode,operand,comment)) { //removed !start_encountered
				addresses[i] = locctr;
				continue;
			}	
			else if(start_encountered && opcode == "START") {
				throw file_parse_exception(string_and_int("Cannot have two START directives on line ", i+1));
			}
			else if(!start_encountered && !is_blank_or_comment(label,opcode,operand,comment)) { 
				throw file_parse_exception(string_and_int("Non-empty line found before START directive on line ", i+1));
			}
			else if(opcode == "BYTE") {
				addresses[i] = locctr;
				locctr += handle_byte(operand, i);
				continue;	
			}	
			else if(opcode == "WORD") {
				addresses[i] = locctr;
				locctr += 3;
				continue;
			}
			else if(opcode == "BASE") {
				addresses[i] = locctr;
				base_enabled = true;	
				continue;
			}
			else if(opcode == "NOBASE") {
				addresses[i] = locctr;
				base_enabled = false;	
				continue;
			}
			else if(opcode == "RESB" || opcode == "RESW") {
				addresses[i] = locctr;
				locctr += add_bytes(opcode,operand, i);
				continue;
			}
			else if(opcode == "END") {
				end_encountered = true;
				addresses[i] = locctr;

				// compared end label with start label
				// throw exception if not the same
				if(get_uppercase(operand) != get_uppercase(start_label))
					throw file_parse_exception(string_and_int("End label does not match the start label on line ", i+1));

				continue;
			}
			else if(opcode == "EQU") {
				addresses[i] = locctr;
				symtab_data = handle_equ(label,operand,i,symtab_data);	
				continue;
			}
			else {
				addresses[i] = locctr;
				locctr += opcode_table.get_instruction_size(opcode);
			}
		}
		// check if start has ever been encountered
		// if not, throw and exception
		if(!start_encountered)
			throw file_parse_exception("No START directive was found.");

		// same as above but for the END directive
		if(!end_encountered)
			throw file_parse_exception("No END directive was found.");

		write_to_file(raw_filename, the_data, addresses);

	}

	catch (file_parse_exception excpt) {

		cout << excpt.getMessage() << endl;
		exit(1);

	}
	catch (opcode_error_exception e) {
	
		cout << e.getMessage() << endl;
		exit(1);

	}
	catch (symtab_exception e) {
		cout << e.get_message() << endl;
		exit(1);
	}
	return 0;
}

string get_raw_filename(string filename) {

	char file_buffer[100];
	strcpy(file_buffer, filename.c_str());
	char* ptr = strtok(file_buffer,".");
	return ptr;

}

bool is_blank_or_comment(string lb, string op, string oper, string comm) {
         bool blank_or_comment = true;
 
        if(!lb.empty()) {
                if(lb[0] != '.') { 
			return false; 
		}
		else { 
			return true; 
		}
	}
        if(!op.empty()) {
                if(op[0] != '.') { 	
			return false; 
		}
		else { 
			return true; 
		}
	}
        if(!oper.empty()) {
                if(oper[0] != '.') { 
			return false; 
		} 
		else { 
			return true; 
		}
	}
        if(!comm.empty()) {
                if(comm[0] != '.') { 
			return false; 
		} 
		else { 
			return true; 
		}
	}

	return blank_or_comment;
}


bool is_blank(string lb, string op, string oper, string comm) {
	return (lb.empty() && op.empty() && oper.empty() && comm.empty());
}


unsigned int handle_start(string oper, int i) {
	if(oper.empty()) {
		throw file_parse_exception(string_and_int("No operand found for START directive on line ", i+1));
	}
	else if(oper[0] == '$') {
		oper.erase(0,1);
		return hex_to_int(oper);
	}
	else if(oper[0] == '#') {
		oper.erase(0,1);
		return decimal_to_int(oper);
	}
	else if(isdigit(oper[0])) {
		return decimal_to_int(oper);
	}
	else
		throw file_parse_exception(string_and_int("START directive operand is not a hexadecimal value on line ", i+1));
}

symtab handle_equ(string l, string oper, int i, symtab s) {
	int value;
	if(l.empty()) {
		throw file_parse_exception(string_and_int("No label found for EQU directive on line ", i+1));
	}
	else if(oper.empty()) {
		throw file_parse_exception(string_and_int("No operand found for EQU directive on line ", i+1));
	}
	else if(isalpha(oper[0])) {
		if(!s.symbol_exists(oper)) // if symbol is a forward reference
			;// Do nothing until pass two
	}
	else if(oper[0] == '$') {
		oper.erase(0,1);
		value = hex_to_int(oper);
		s.add_symbol_to_map(l, value, i);
	}
	else if(oper[0] == '#') {
		oper.erase(0,1);
		value = decimal_to_int(oper);
		s.add_symbol_to_map(l, value, i);
	}
	else if(isdigit(oper[0])) {
		value = decimal_to_int(oper);
		s.add_symbol_to_map(l, value, i);
	}
	else
		throw file_parse_exception(string_and_int("Invalid operand found for EQU directive on line ", i+1));

	return s;
}

int handle_byte(string s, int i) {

         if(s.empty())
                throw file_parse_exception(string_and_int("No operand found for BYTE directive on line ", i+1)); 
 
         int count = 0;
         bool in_quotes = false;
 
         for(int i=0; i<s.size(); i++) {
                 if(s[i] == '\'' && !in_quotes)
                         in_quotes = true;
                 else if(s[i] == '\'' && in_quotes)
                         in_quotes = false;
                 else if(in_quotes)
                         count++;
         }
 
         if(tolower(s[0]) == 'x') {
                 if(count % 2 != 0)
                        throw file_parse_exception(string_and_int("Odd number of characters for BYTE directive on line ", i+1)); 
                 return count / 2;
         }
         else if(tolower(s[0]) == 'c')
                 return count;
         else
                 throw file_parse_exception(string_and_int("Operand is not hex or char on line ", i+1));
}

unsigned int hex_to_int(string l) {
	unsigned int integer_val;
	stringstream ss;
	ss << hex << l;
	ss >> integer_val;
	return integer_val;
}

unsigned int add_bytes(string op, string oper, int i) {
	if(oper.empty())
		throw file_parse_exception(string_and_int("No operand found for RESB or RESW directive on line ", i+1));
	if(op == "RESW") {
		if(oper[0] == '$')
			return hex_to_int(oper) * 3;
		else if(isdigit(oper[0]))
			return decimal_to_int(oper) * 3;
		else if (oper[0] == '#')
			return decimal_to_int(oper) * 3;
		else
			throw file_parse_exception(string_and_int("Invalid operand for RESB or RESW directive on line ", i+1));
	}
	else if(op == "RESB") {
		if(oper[0] == '$')
			return hex_to_int(oper);
		else if(isdigit(oper[0]) || oper[0] == '#')
			return decimal_to_int(oper);
		else
			throw file_parse_exception(string_and_int("Invalid operand for RESB or RESW directive on line ", i+1));
	}
	return 0;
}

int decimal_to_int(string l) {
	int integer_val = atoi(l.c_str());
	return integer_val;
}

void print_d(string filename, file_parser p, unsigned int* a) {
	string title = "**"+filename+"**";
	int max_width = 57;
	int setw_offset = (max_width/2) + (title.length()/2);
	string label, opcode, operand, comment;

	cout 	<< "\n"
		<< setw(setw_offset) << right << title << endl << endl;

	cout	<< setw(7) << right << "Line#" 
		<< setw(12) << right << "Address"
		<< setw(10) << right << "Label"
		<< setw(14) << right << "Opcode"
		<< setw(14) << right << "Operand" << endl;

	cout	<< setw(7) << right << "=====" 
		<< setw(12) << right << "======="
		<< setw(10) << right << "====="
		<< setw(14) << right << "======"
		<< setw(14) << right << "=======" << endl;

	for(int i = 0; i < p.size(); i++) {
			label = p.get_token(i,0);
			opcode = p.get_token(i,1);
			operand = p.get_token(i,2);
			comment = p.get_token(i,3);

		cout 	<< setw(7) << right << dec << i+1 
			<< setw(7) << right << " ";
		cout 	<< setw(5) << hex << uppercase << setfill('0') << a[i] << setfill(' ');
		cout	<< setw(10) << right << label
			<< setw(14) << right << opcode
			<< setw(14) << right << operand << endl;
	}
}

// write data to file
void write_to_file(string filename, file_parser p, unsigned int* a) {

	// initialize variables
	string title = "**"+filename+".asm**";
	int max_width = 57;
	int setw_offset = (max_width/2) + (title.length()/2);
	string label, opcode, operand, comment;
	string lis_file = filename+ ".lis";

	// initialize file variable and create file
	ofstream file(lis_file.c_str(), fstream::out);
					
	file 	<< "\n"
		<< setw(setw_offset) << right << title << endl << endl;

	file	<< setw(7) << right << "Line#" 
		<< setw(12) << right << "Address"
		<< setw(10) << right << "Label"
		<< setw(14) << right << "Opcode"
		<< setw(14) << right << "Operand" << endl;

	file	<< setw(7) << right << "=====" 
		<< setw(12) << right << "======="
		<< setw(10) << right << "====="
		<< setw(14) << right << "======"
		<< setw(14) << right << "=======" << endl;

	for (int i = 0; i < p.size(); i++ ) {

		label = get_uppercase(p.get_token(i,0));
		opcode = get_uppercase(p.get_token(i,1));
		operand = get_uppercase(p.get_token(i,2));
		comment = get_uppercase(p.get_token(i,3));
		
		file 	<< setw(7) << right << dec << i+1 
			<< setw(7) << right << " ";
		file 	<< setw(5) << hex << uppercase << setfill('0') << a[i] << setfill(' ');
		file	<< setw(10) << right << label
			<< setw(14) << right << opcode
			<< setw(14) << right << operand << endl;
		
	}

	
	// close file when done
	file.close();
}

// return capitalized string
string get_uppercase(string text) {

	transform(text.begin(), text.end(), text.begin(), ::toupper);
	return text;

}

// combines a string and int and returns a string
// used primarily for the exceptions and line numbers
string string_and_int(string s, unsigned int l) {

	std::ostringstream e;
	e << s << l;
	return e.str();

}
