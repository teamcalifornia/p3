/*
    Mark Mandocdoc, Malik McElroy, Xiaojia Song, Mohamed Sharif 
    masc0882 
    Team California
    prog3
    CS530, Spring 2016
 */

#include "file_parser.h"
#include "file_parse_exception.h"

using namespace std;

file_parser::file_parser(string fileName) {
	FILE_NAME = fileName;
}

file_parser::~file_parser() {

}

void file_parser::read_file() {

	ifstream infile;

	infile.open(FILE_NAME.c_str(),ios::in);
	if(infile.good() == false) {
		throw file_parse_exception("Error: File '" +FILE_NAME+ "' not found.");
	}
	string lineBuffer;
	while(!infile.eof()) {
		getline(infile, lineBuffer);
		contents.push_back(lineBuffer);
	}
	contents.pop_back();
	infile.close();


	for(int i = 0; i < size(); i++) {
	
		char stringBuffer[100];
		strcpy(stringBuffer, contents[i].c_str());
		line newLine;

		try {
			newLine.label = make_label(stringBuffer);
			newLine.label_full = make_label_full(stringBuffer);
			newLine.opcode = make_opcode(stringBuffer);
			newLine.operand = make_operand(stringBuffer);
			newLine.comment = make_comment(stringBuffer);
		}
		catch (file_parse_exception e) {
			std::ostringstream error_line_number;
			error_line_number << e.getMessage() << " on line " << i + 1;
			throw(file_parse_exception(error_line_number.str()));
		}

		LINES.push_back(newLine);
	}

}

string file_parser::get_token(unsigned int row, unsigned int col) {
	string theToken;

	switch(col) {
		case 0:
			theToken = LINES[row].label;
			break;

		case 1:
			theToken = LINES[row].opcode;
			break;

		case 2:
			theToken = LINES[row].operand;
			break;

		case 3:
			theToken = LINES[row].comment;
			break;

		default:
			theToken = "";
	}
	return theToken;
}

void file_parser::print_file() {
	for(int i = 0; i < size(); i++) {
		
		cout << setw(3) << left << i <<
			setw(14) << left << LINES[i].label <<
			setw(14) << left << LINES[i].opcode <<
			setw(30) << left << LINES[i].operand << 
			setw(50) << left << LINES[i].comment << endl;
		
	}
}

int file_parser::size() {
	return contents.size();
}

/* Private Methods */

string file_parser::make_label(char* s) {

	int LABEL_CHAR_LIMIT = 8;

	int count = 0;
	int index = 0;
	char buff[100];

	strcpy(buff, s);
	string label;

	if(s[0] == '.' || s[0] == NULL) return "";
	
	while (!isspace(buff[index])) {
		if(isalnum(buff[index])) {
			label.push_back(buff[index]);
			count++;	
			index++;
		}
		else {
			throw(file_parse_exception("Error: Label contains illegal characters"));	
		}
		if(count >= LABEL_CHAR_LIMIT) break;
	}
	return label;

}

string file_parser::make_label_full(char* s) {

	int index = 0;
	char buff[100];

	strcpy(buff, s);
	string label_full;

	if(s[0] == '.' || s[0] == NULL) return "";
	
	while (!isspace(buff[index]) && buff[index] != NULL) {
		if(isalnum(buff[index])) {
			label_full.push_back(buff[index]);
			index++;
		}
		else {
			throw(file_parse_exception("Error: Label Full contains illegal characters"));	
		}
	}
	return label_full;

}

string file_parser::make_opcode(char* s) {
	
	int currentColumn = 0;
	string opcode = "";
	int i = 0;

	while(s[i] != NULL && currentColumn != 1 && s[i] != '.') {
		while(isalnum(s[i])) i++;
		while(s[i] == ' ' || s[i] == '\t'){ i++; }
		currentColumn++;
	}
	if( s[i] == '.' || s[i+1] == '.' ) return "";

	while(s[i] != NULL) {
		opcode.push_back(s[i]);
		i++;

		if(s[i] == ' ' || s[i] == '\t') break;
	}

	return opcode;

}

string file_parser::make_operand(char* s) {

	bool inQuote = false;
	int currentColumn = 0;
	string operand = "";
	int i = 0;

	while(s[i] != NULL && currentColumn != 2 && s[i] != '.') {
		while((!isspace(s[i]))) i++;
		while(s[i] == ' ' || s[i] == '\t'){ i++; }
		currentColumn++;
	}
	if( s[i] == '.' || s[i+1] == '.' ) return "";

	while(s[i] != NULL) {
		if(s[i] == '\''  && inQuote == false) inQuote = true;
		else if(s[i] == '\'' && inQuote == true && (s[i+1] == ' ' || s[i+1] == '\t' || s[i+1] == NULL)) inQuote = false;
		operand.push_back(s[i]);
		i++;

		if((s[i] == ' ' || s[i] == '\t') && inQuote == false) break;
	}

	if(inQuote == true) { 
		throw file_parse_exception("Error: Expected quotation at operand");
	}

	return operand;

}

string file_parser::make_comment(char* s) {

	bool inQuote = false;
	int currentColumn = 0;
	int i = 0;

	while(s[i] != NULL && currentColumn != 2 && s[i] != '.') {
		while(!isspace(s[i])) i++;
		while(s[i] == ' ' || s[i] == '\t'){ i++; }
		currentColumn++;
	}

	while(s[i] != NULL && s[i] != '.' && s[i+1] != '.') {
		if(s[i] == '\''  && inQuote == false) inQuote = true;
		else if(s[i] == '\'' && inQuote == true && (s[i+1] == ' ' || s[i+1] == '\t' || s[i+1] == NULL)) inQuote = false;
		i++;
		if((s[i] == ' ' || s[i] == '\t') && inQuote == false) break;
	}

	if(inQuote == true) { 
		throw file_parse_exception("Error: Expected quotation at operand");
	}


	while(isalpha(s[i]) && s[i] != '.') i++;
	while((s[i] == ' ' || s[i] == '\t') && s[i] != NULL) i++;
	currentColumn++;

	if(s[i] != '.' && s[i] != NULL && currentColumn == 3 && isalpha(s[i])) 
		throw file_parse_exception("Error: Comment missing period");
 
	bool inComment = false;
	string comment = "";
	
	while(s[i] != NULL) {
		if(s[i] == '.')
			inComment = true;
		if(inComment)
			comment.push_back(s[i]);
		i++;
	}

	return comment;

}

