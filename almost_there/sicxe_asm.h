/*  Mark Mandocdoc, Malik McElroy, Xiaojia Song, Mohamed Sharif
    masc0882
    Team California
    prog3
    CS530, Spring 2016
*/

#ifndef SICXE_ASM_H
#define SICXE_ASM_H

#include <algorithm>
#include <string>
#include <cstring>
#include <sstream> // required for string to integer conversion
#include <map>
#include <vector>
#include <iostream>
#include <fstream> // required for writing to file
#include "file_parser.h"
#include "file_parse_exception.h"
#include "opcodetab.h"
#include "opcode_error_exception.h"
#include "symtab.h"
#include "symtab_exception.h"

using namespace std;

int main (int, char** );

// returns file name without extension
string get_raw_filename(string);

// previews what will be printed into the list file
void print_d(string, file_parser, unsigned int*);

// handles specific cases
unsigned int handle_start(string, int);
int handle_byte(string, int);

// handles EQU directive and returns a symtab
symtab handle_equ(string,string,int,symtab);

// checks if the line is blank or has a comment
bool is_blank_or_comment(string,string,string,string);
bool is_blank(string, string, string, string);

// returns the int or hex value from a string
unsigned int hex_to_int(string);
int decimal_to_int(string);

// parses the operand and calculates bytes needed to add
unsigned int add_bytes(string, string, int);

// writes processed data to the lis file
void write_to_file(string, file_parser, unsigned int*);

// returns a capitalized string
string get_uppercase(string);
bool is_blank(string, string, string, string);

// takes a string and int and returns a string with both
string string_and_int(string, unsigned int);

#endif
